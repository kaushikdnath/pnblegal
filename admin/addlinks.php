<?php 
include_once './header.php';
include_once 'messages/alertClass.php';
$message=new alertClass();
if(isset($_POST["AddLink"])){
    include_once 'functions/gump.class.php';
    $gump = new GUMP();
    $gump->validation_rules(array(
        'linktitle'    => 'required',
        'link'    => 'required',
        'linkparent'    => 'required'
    ));
    $gump->filter_rules(array(
        'linktitle'    => 'trim|sanitize_string',
        'link'    => 'trim|sanitize_string',
        'linkparent'    => 'trim|sanitize_string'
    ));
    $validated_data = $gump->run($_POST);
    if($validated_data === false) {
      $alert=$message->getAlert($gump->get_readable_errors(true),"error");
    } else {
            unset($validated_data['submit']);
            define('pnbclassconst', TRUE);
            include_once 'classes/link.php';
            $linkClass=new linksClass();
            $alert=$linkClass->addLink($validated_data);
    }
}
?>
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-8 col-md-offset-2 main">
          <h1 class="page-header">Add New Links</h1>
          <?php if(isset($alert)){echo $alert;}?>
          <form method="post" action="<?php echo $_SERVER["PHP_SELF"] ?>">
              <div class="form-group">
                  <label>Select Parent*</label>
                  <select name="linkparent" class="form-control">
                    <option value='GST'>GST</option>
                    <option value='Custom'>Custom</option>
                    <option value='RBI'>RBI</option>
                  </select>
              </div>
              <div class="form-group">
                  <label>Link Title*</label>
                  <input type="text" name="linktitle" value="" class="form-control">
              </div>
              <div class="form-group">
                  <label>Link*</label>
                  <input type="text" id="link" name="link" value="" class="form-control">
              </div>
              <input type="submit" id="submit" name="AddLink" value="Add link" class="btn btn-primary">
          </form>
        </div>
      </div>
    </div>


    <?php 
        define('pnbclassconst', TRUE);
        include_once 'classes/link.php';
        $linkClass=new linksClass();
        if(isset($_POST["delete"])){
            $deleteid=$_POST["did"];
            if($deleteid==""){
                
            }
            else
            {
                $alert='';
                $alert.="<form method=post>";
                $alert.= $message->getAlert("Are you sure you want to delete 
                    <input type='hidden' name=cdid value='$deleteid'>
                    <input type='submit' name='cdel' class='btn btn-sm btn-danger' value='Yes'> &nbsp; 
                    <input type='submit' name='ndel' class='btn btn-sm btn-primary' value='No'>", "warning");
                $alert.= "</form>";
            }
        }

        if(isset($_POST["cdel"])){
            $confirmId=$_POST["cdid"];
            if(!$confirmId==""){
                $alert=$linkClass->deleteLink($confirmId);
            }
        }
        $linkList=$linkClass->listLinks();
    ?>
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-8 col-md-8 col-md-offset-2  main">
          <h1 class="page-header">Links List</h1>
          <?php if(isset($alert)){echo $alert;}?>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Parent</th>
                  <th>Title</th>
                  <th>URL</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                  <?php
                  $i=1;
                    if(count($linkList)){
                        foreach ($linkList as $value) {
                            echo "<tr>
                                <td>$i</td>
                                <td>$value[parent]</td>
                                <td>$value[title]</td>
                                <td><a href='$value[href]' target='_blank'>$value[href]</a></td>
                                <td><a class='btn btn-warning btn-sm' href='edit-pages.php?eid=$value[id]'>Edit</a>&nbsp;&nbsp;&nbsp;
                  <form action='$_SERVER[PHP_SELF]' method=post>
<input type=hidden value='$value[id]' name='did' id='did'>                      
<input type=submit name=delete value=Delete class='btn btn-danger btn-sm'></form></td>
                                </tr>";
                            $i++;
                        }
                    }
                  ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script> 
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script> 
    <!--<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>-->
    <script>
    // $(document).ready(function() {
    //     $('#summernote').summernote({
    //       height: 300,                 // set editor height
    //       minHeight: null,             // set minimum height of editor
    //       maxHeight: null,             // set maximum height of editor
    //       focus: true                  // set focus to editable area after initializing summernote
    //     });
    // });
  </script>
  </body>
</html>
