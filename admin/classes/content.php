<?php
if (!defined('pnbclassconst')){die('You cannot access this file directly!');}
require_once( dirname( __FILE__ ) . '/../functions/connectionClass.php' );
require_once( dirname( __FILE__ ) . '/../messages/alertClass.php' );

class contentsClass extends connectionClass{
    
    public function addContent($data){
        $message=new alertClass();
        $rowdata["title"]=$this->real_escape_string($data["contenttitle"]);
        $rowdata["href"]=$this->real_escape_string($data["contentdetail"]);
        if($this->dulicateContent($rowdata["href"])<1){
            $keys=array_keys($rowdata);
            $values=array_values($rowdata);
            date_default_timezone_set ("Asia/Kolkata");
            $table="contents";
            $query='INSERT INTO '.$table.' ('.implode(', ', $keys).') VALUES ("'.implode('","', $values).'")';
            $result=  $this->query($query) or die($this->error);
            if($result){
                unset($_POST);
                return $message->getAlert("Content is added ", "success");
            }
            else
            {
                return $message->getAlert("Error while adding Content", "error");
            }
        }
        else
        {
            return $message->getAlert("Already Exists", "error");
        }
    }
    
    
    public function dulicateContent($name){
        $check="select * from Contents where href='$name'";
        $result=  $this->query($check);
        $count=  $result->num_rows;
        if($count < 1){return 0;}else{return $count;}
    }
    
  private function slug($string){
       $string = strtolower(trim($string));
    $string = str_replace("'", '', $string);
    $string = preg_replace('#[^a-z\-]+#', '-', $string);
    $string = preg_replace('#_{2,}#', '_', $string);
    $string = preg_replace('#_-_#', '-', $string);
    $string = preg_replace('#(^_+|_+$)#D', '', $string);
    return preg_replace('#(^-+|-+$)#D', '', $string);
}

    public function listContents(){
        $list="select * from Contents Order By parent,Id ";
        date_default_timezone_set ("Asia/Kolkata");
        $result=  $this->query($list);
        $count=  $result->num_rows;
        if($count < 1){}else{
            while($row= $result->fetch_array(3)){
                $arr[]= $row;
            }
            return $arr;
        }
    }
    
   
    public function particularContent($id) {
        if(is_numeric($id)){
        $list="select * from Contents where id='$id'";
        $result=  $this->query($list);
        $count=  $result->num_rows;
        if($count < 1){}else{
            while($row= $result->fetch_array(3)){
                return $row;
            }
        }
        }
    }
    
    public function particularContentSlug($id) {
        $list="select * from Contents where href='$id'";
        $result=  $this->query($list);
        $count=  $result->num_rows;
        if($count < 1){}else{
            while($row= $result->fetch_array(3)){
                return $row;
            }
        }
    }
    
public function updateContent($data,$id){
    $message=new alertClass();
    $rowdata["title"]=$this->real_escape_string($data["Contenttitle"]);
    $rowdata["href"]=$this->slug($data["Content"]);
    foreach ($rowdata as $key => $value) {
        $value="'$value'";
        $updates[]="$key=$value";
    }
    $imploadAray=  implode(",", $updates);
    $query="Update Contents Set $imploadAray Where id='$id'";
    $result=  $this->query($query) or die($this->error);
        if($result){
            return $message->getAlert("Content is updated", "success");
        }
        else
        {
            return $message->getAlert("Error while updating Content", "error");
        }  
}

public function deleteContent($id){
    $delete="Delete from Contents where id='$id'";
    $result=  $this->query($delete);
    $message=new alertClass();
        if($result){
            return $message->getAlert("Content is deleted", "success");
        }
        else
        {
            return $message->getAlert("Error while deleting Content", "error");
        }  
}
}
