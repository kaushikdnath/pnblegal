<?php
if (!defined('pnbclassconst')){die('You cannot access this file directly!');}
require_once( dirname( __FILE__ ) . '/../functions/connectionClass.php' );
require_once( dirname( __FILE__ ) . '/../messages/alertClass.php' );

class linksClass extends connectionClass{
    
    public function addLink($data){
        $message=new alertClass();
        $rowdata["title"]=$this->real_escape_string($data["linktitle"]);
        $rowdata["href"]=$this->real_escape_string($data["link"]);
        $rowdata["parent"]=$this->real_escape_string($data["linkparent"]);
        if($this->dulicateLink($rowdata["href"])<1){
            $keys=array_keys($rowdata);
            $values=array_values($rowdata);
            date_default_timezone_set ("Asia/Kolkata");
            $table="links";
            $query='INSERT INTO '.$table.' ('.implode(', ', $keys).') VALUES ("'.implode('","', $values).'")';
            $result=  $this->query($query) or die($this->error);
            if($result){
                unset($_POST);
                return $message->getAlert("Link is added ", "success");
            }
            else
            {
                return $message->getAlert("Error while adding link", "error");
            }
        }
        else
        {
            return $message->getAlert("Already Exists", "error");
        }
    }
    
    
    public function dulicateLink($name){
        $check="select * from links where href='$name'";
        $result=  $this->query($check);
        $count=  $result->num_rows;
        if($count < 1){return 0;}else{return $count;}
    }
    
  private function slug($string){
       $string = strtolower(trim($string));
    $string = str_replace("'", '', $string);
    $string = preg_replace('#[^a-z\-]+#', '-', $string);
    $string = preg_replace('#_{2,}#', '_', $string);
    $string = preg_replace('#_-_#', '-', $string);
    $string = preg_replace('#(^_+|_+$)#D', '', $string);
    return preg_replace('#(^-+|-+$)#D', '', $string);
}

    public function listLinks(){
        $list="select * from links Order By parent,Id ";
        date_default_timezone_set ("Asia/Kolkata");
        $result=  $this->query($list);
        $count=  $result->num_rows;
        if($count < 1){}else{
            while($row= $result->fetch_array(3)){
                $arr[]= $row;
            }
            return $arr;
        }
    }
    
   
    public function particularLink($id) {
        if(is_numeric($id)){
        $list="select * from links where id='$id'";
        $result=  $this->query($list);
        $count=  $result->num_rows;
        if($count < 1){}else{
            while($row= $result->fetch_array(3)){
                return $row;
            }
        }
        }
    }
    
    public function particularLinkSlug($id) {
        $list="select * from links where href='$id'";
        $result=  $this->query($list);
        $count=  $result->num_rows;
        if($count < 1){}else{
            while($row= $result->fetch_array(3)){
                return $row;
            }
        }
    }
    
public function updateLink($data,$id){
    $message=new alertClass();
    $rowdata["title"]=$this->real_escape_string($data["linktitle"]);
    $rowdata["href"]=$this->slug($data["link"]);
    foreach ($rowdata as $key => $value) {
        $value="'$value'";
        $updates[]="$key=$value";
    }
    $imploadAray=  implode(",", $updates);
    $query="Update links Set $imploadAray Where id='$id'";
    $result=  $this->query($query) or die($this->error);
        if($result){
            return $message->getAlert("Link is updated", "success");
        }
        else
        {
            return $message->getAlert("Error while updating link", "error");
        }  
}

public function deleteLink($id){
    $delete="Delete from links where id='$id'";
    $result=  $this->query($delete);
    $message=new alertClass();
        if($result){
            return $message->getAlert("Link is deleted", "success");
        }
        else
        {
            return $message->getAlert("Error while deleting link", "error");
        }  
}
}
