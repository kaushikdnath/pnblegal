<?php 
include_once './header.php';
include_once 'messages/alertClass.php';
$message=new alertClass();
if(isset($_POST["submit"])){
    include_once 'functions/gump.class.php';
    $gump = new GUMP();
    $gump->validation_rules(array(
        'contenttitle'    => 'required',
        'contentdetail'   => 'required'
));
    $gump->filter_rules(array(
        'contenttitle'    => 'trim|sanitize_string',
        'contentdetail'   => 'trim|sanitize_string'
));
    $validated_data = $gump->run($_POST);
    if($validated_data === false) {
    $alert=$message->getAlert($gump->get_readable_errors(true),"error");
} else {
        unset($validated_data['submit']);
        define('pnbclassconst', TRUE);
        include 'classes/content.php';
        $contentClass=new contentsClass();
        $alert=$contentClass->addContent($validated_data);
}
}
?>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-8 col-md-offset-2 main">
          <h1 class="page-header">Add Pages</h1>
          <?php echo $alert; ?>
          <form method="post" action="<?php echo $_SERVER["PHP_SELF"] ?>" enctype="multipart/form-data">
              <div class="form-group">
                  <label>Select Title*</label>
                  <select name="contenttitle" class="form-control">
                    <option value='Litigation'>GST</option>
                    <option value='CONSULTING &amp; ADVISORY,'>CONSULTING & ADVISORY,</option>
                    <option value='RBI'>COMPLIANCE</option>
                  </select>
              </div>
              <div class="form-group">
                  <label>Page Details*</label>
                  <textarea id="summernote" name="PageDetails"><?php echo $_POST["PageDetails"] ?></textarea>
              </div>
              <input type="submit" id="submit" name="submit" value="Add Page" class="btn btn-primary">
          </form>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script> 
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script> 
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>
    <script>
    $(document).ready(function() {
        $('#summernote').summernote({
  height: 300,                 // set editor height
  minHeight: null,             // set minimum height of editor
  maxHeight: null,             // set maximum height of editor
  focus: true                  // set focus to editable area after initializing summernote
});
    });
  </script>
  </body>
</html>
